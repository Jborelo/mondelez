#!/usr/bin/python3

import json
import subprocess
#from app import callR


#------------------------------------------------------
def callR(action, params):
    script = action + ".R"
    command = "Rscript"
    cmd=[command, script, params]
    #print ("-----   call : " + str(cmd))
    out_bytes = subprocess.check_output(cmd)
    out_text = out_bytes.decode('utf_8')
    #print("**** returned fRom call: " + str(out_text))
    return out_text


print("MasterR6 here")

jss="q6.txt"
if not os.path.isfile(jss):
    print("Can not found: " + str(jss)) 
    sys.exit(0)


jsob=json.loads(jss)
ss = json.dumps(jsob)

callR("Q6", ss)

