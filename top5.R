library(readr)
library(ggplot2)
library(reshape)
library(sqldf)

#setwd("D:/R projects/2017/Bi Bot")
df <- read_delim("./PGbibot_random.csv", ";", escape_double = FALSE, trim_ws = TRUE)


top5 <- df[1:5,]
top5$Category<-as.factor(top5$Category)
top5$Touches<-as.numeric(top5$Touches)


xxx<-ggplot(top5, aes(x = reorder(Category, -Touches), y = Touches, fill = Category)) + 
  geom_bar(stat = "identity")+  xlab("Category") + ylab("Touches")+
  theme(strip.text.x = element_blank(),
        plot.background = element_rect(fill = "#505050"),
        panel.background = element_rect(fill = "#505050"),
        axis.title.x = element_text(color = "#D0D0D0", face = "bold"),
        axis.title.y = element_text(color = "#D0D0D0", face = "bold"),
        panel.grid.major = element_line(color = "#505050"),
        axis.text = element_text(color = "#D0D0D0", face = "bold", angle = 45, hjust = 1),
        legend.position=c(.85,.8)
  )


ggsave(filename="top5plot.jpg", plot=xxx, width = 15, height = 15, units = "cm")

full_ans <- paste(top5$Category, " has ", top5$Touches, " value of Touches. ", sep = "")
cat(full_ans)
