import os
import datetime
import json
import subprocess
import logging
import time


from flask import Flask
from flask import request
from flask import make_response


#--------------------------------------------------------------------------------------------------
# APP_VERSION="1.0"
#   logging to  file added
# APP_VERSION = "1.1"
#   https added
# APP_VERSION = "1.3" - https disabled
# APP_VERSION = "1.5" = https enabled mond.hopto.org:5000
# APP_VERSION = "1.6" - long request test
#
#--------------------------------------------------------------------------------------------------

APP_PORT = 5000
APP_VERSION = "Mondelez 1.66"
LOG_FILE_NAME = "mondelez.log"


#create simple file logger
fh = logging.FileHandler(LOG_FILE_NAME)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)

_lgr_ = logging.getLogger("m")
_lgr_.setLevel(logging.DEBUG)
_lgr_.addHandler(fh)

# create  server object
app = Flask(__name__)


#----------------------------------------------------
def log(msg):
    #print (msg)
    _lgr_.debug(msg)


#------------------------------------------------------
def getVersion():
    return APP_VERSION


#------------------------------------------------------
@app.route('/longr', methods=['POST'])
def longr2():
    print ("Start in longr2")
    ii=1
    for ii in range (0, 1000000):
        s = str(ii)
    print ("Done in longr2")


#------------------------------------------------------
@app.route('/longr0', methods=['POST'])
def longr0():
    log("in longR 4")
    jsnreq = request.get_json(silent=True, force=True)
    ss = str(json.dumps(jsnreq, indent=4)) 
    log("------" + ss + "---" )
    time.sleep(5)
    return ss  


#------------------------------------------------------
@app.route('/version', methods=['GET'])
def version():
    return getVersion()


#------------------------------------------------------
@app.route('/webhook', methods=['GET'])
def webhook_get():
    log("  --- POST only !! ---")
    return "Post only !!"


#------------------------------------------------------
@app.route('/webhook', methods=['POST'])
def webhook():
    #log("  --- in webhook() ---")
    jsnreq = request.get_json(silent=True, force=True)
    #log("------" + str(json.dumps(jsnreq, indent=4)) + "---" )

    # get action and parameters
    action, params, completed = extract(jsnreq)
    #log("------ Action is Complted: "+ str(completed))
    #log("------ action: "  + str(action) +" params: \n" + str(json.dumps(params, indent=4)) + "---" )
    log("----------------------------------------------------------------------- Action: "  + str(action)  + "---" )

    resp=""
    if completed:
        rresult = callR(action, json.dumps(params))
        #log ("*****  Returned  fRom: " + str(rresult) + " *****")

        # enclose text returned from R script into json to be returned to DialogFlow 
        jsonToReturn = returnSpeech(rresult)

        #ss = json.dumps(jsonToReturn, indent=4)
        ss = json.dumps(jsonToReturn)
        #log ("-----    " + ss)
        resp = make_response(ss)
        resp.headers['Content-Type'] = 'application/json'
    else:
        log ("!! Action incomplete! ")

    return resp


#------------------------------------------------------
def callR(action, params, script="Rscript"):
    script = action + ".R"
    command = "Rscript"
    cmd=[command, script, params]
    log ("-----   call : " + str(cmd))
    #out_bytes = subprocess.check_output(cmd, stderr=subprocess.STDOUT)
    out_bytes = subprocess.check_output(cmd)
    out_text = out_bytes.decode('utf_8')
    log("")
    log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> returned fRom call: " + str(command) + "  " + str(script))
    log(str(out_text))
    log("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")
    log("")
    return out_text


#------------------------------------------------------
#      {
#      "id": "53c163e3-69e5-4db2-9b37-2fba2e259efb",
#      "timestamp": "2017-11-16T23:01:00.684Z",
#      "lang": "en",
#      "result": {
#          "source": "agent",
#          "resolvedQuery": "Show me best performing retailers",
#           "action": "Q2",
#           "actionIncomplete": false,
#           "parameters": {},
#           "contexts": [],
#------------------------------------------------------
def extract(jsn):
    """
    From given json extracts action and parameters
    """
    res = jsn["result"]
    action = res["action"]
    #params =jsn
    params = res["parameters"]
    actionCompleted = not res["actionIncomplete"]
    return (action, jsn, actionCompleted) #` the whole json is beeing send  as parameters
    #return (action, params, actionCompleted)


#------------------------------------------------------------
def returnSpeech(speech, displayText=None, contexts=[], followUpEvent={}):
    if (displayText is None):
        displayText = speech

    result = {
        "speech": speech,
        "displayText": displayText,
        "contextOut": contexts,
        "source": "webhook-createEvent"
    }

    if len(followUpEvent) > 0:
        result["followupEvent"] = followUpEvent

    return result


if __name__ == "__main__":
    port = int(os.getenv('PORT', APP_PORT))
    log("Starting app on port %d" % port)
    log(datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%dT%H:%M:%SZ'))
    cert=('/etc/letsencrypt/live/mond.hopto.org/fullchain.pem', '/etc/letsencrypt/live/mond.hopto.org/privkey.pem')
    #app.run(port=port, host='0.0.0.0', ssl_context=cert, threaded=True)
    #app.run(debug=True, port=port, host='0.0.0.0', ssl_context=cert, processes=5)
    #app.run(debug=True, port=port, host='0.0.0.0', ssl_context=cert, threaded=True)
    app.run(debug=True, port=port, host='0.0.0.0', ssl_context=cert)
    #app.run(debug=True, port=port, host='0.0.0.0', ssl_context='adhoc')


