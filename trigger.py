#!/usr/bin/python3

# run_max.py
import subprocess

# Define command and arguments
command = 'Rscript'
#define path to script
path2script = 'Q2.R' #to be extracted from JSON

#parameters setup
# Q1 no parameters
# Q2 no parameters
# Q3 - product
# Q4 - no parameters
# Q5 - no parameters
# Q6 - column, product


# Variable number of args in a list   to be extracted from JSON
args = ['Mar_17_IR_Tied_North', 'Five Star' ]
#args = ['alcohol']
# Build subprocess command
cmd = [command, path2script]  + args

print(cmd)

# check_output will run the command and store to result
x = subprocess.check_output(cmd, universal_newlines=True)

print(x)
