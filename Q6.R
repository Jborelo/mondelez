#show chosen data from Brand Pri file
library(readr)
library(sqldf)
library(jsonlite)
library(dplyr)

#setwd("D:/Users/wojciech.koscik/Downloads/Bi Bot")
df <- read_delim("./BrandPri.csv", ";", escape_double = FALSE, trim_ws = TRUE)

myargs <- as.factor(commandArgs(trailingOnly = TRUE))
myargs<-gsub("\n", "", myargs)
#print(myargs[1])A
#print("About to parse json ")
my.JSON<-fromJSON(myargs[1])
#print("Json  parsed ")

result <- my.JSON$result
parameter <- result$parameters
column<-parameter$column
produkt<-parameter$brand

xxx<-df[ , grep( column , names( df ) ) ]
names<-(df$Brand_Sales)
names<-as.data.frame(names)
y<-c(names, xxx)
y<-as.data.frame(y)

bbb <- sqldf(paste("select * from y where names in ('",produkt ,"')", sep = ""))

value<-bbb[,2]
full_ans <- paste("Value of the ", produkt, " in ", column, " equals ", value, sep = "")
cat(full_ans)
