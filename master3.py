#!/usr/bin/python3

import json
import os
import subprocess
from app import callR


print("MasterR6 here. Calling Q6.R with string from q6.txt")

fil = "q3.txt"
if not os.path.isfile(fil):
    print("Can not found: " + str(fil)) 
    sys.exit(0)


with open(fil) as json_file:
    jss = json.load(json_file)

ss = json.dumps(jss)

#ss="""{
#"a": "va",
#"b": "vb"
#}
#"""
callR("Q3", ss)


