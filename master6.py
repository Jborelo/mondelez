#!/usr/bin/python3

import json
import os
import subprocess
#from app import callR


#------------------------------------------------------
def callR(action, params):
    script = action + ".R"
    command = "Rscript"
    cmd=[command, script, params]
    #cmd=["python3", "slave.py", params]
    print ("-----   call : " + str(cmd))
    out_bytes = subprocess.check_output(cmd)
    out_text = out_bytes.decode('utf_8')
    print("")
    print("**** returned fRom call: " + str(out_text))
    return out_text


print("MasterR6 here. Calling Q6.R with string from q6.txt")
fil = "q6.txt"
if not os.path.isfile(fil):
    print("Can not found: " + str(fil)) 
    sys.exit(0)

with open(fil) as json_file:
    jss = json.load(json_file)

ss = json.dumps(jss)

#ss="""{
#"a": "va",
#"b": "vb"
#}
#"""
callR("Q6", ss)


