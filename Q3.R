library(dplyr)
library(jsonlite)
library(ggplot2)
comprss <- function(tx) {
  div <- findInterval(as.numeric(gsub("\\,", "", tx)),
                      c(1, 1e3, 1e6, 1e9, 1e12) )
  paste(round( as.numeric(gsub("\\,","",tx))/10^(3*(div-1)), 2),
        c("","Thousands","Millions","Billions","T")[div] )}


#setwd("D:/Bi Bot/Bi Bot") #path for file with data
df <- read.csv("./Book2.csv", header=TRUE, sep=";") #insert data to R

myargs <- as.factor(commandArgs(trailingOnly = TRUE))
myargs<-gsub("\n", "", myargs)

my.JSON<-fromJSON(myargs[1])

result <- my.JSON$result
parameters <- result$parameters

product<-parameters$product
########## Answer for 10th question ##########
  # preparation of data for analysis
  # Select only the data we are interested in and make the appropriate aggregation

#s.numeric
df$sales_value<-as.numeric(df$sales_value)
  aggdata <-as.data.frame(aggregate(df[which(df$category == product),]$sales_value, 
                                    list(df[which(df$category == product),]$report_year, 
                                         df[which(df$category == product),]$report_mth, 
                                         df[which(df$category == product),]$retailer), sum))
  # Rename columns
  colnames(aggdata) <- c("report_year", "report_mth", "retailer", "sales_value")
  
  # Getting unique names of retailers
  retailers <- as.data.frame(unique(aggdata$retailer))
  
  # Rename column
  colnames(retailers) <- c("retailers")
  
  # Adding new column with zeros.
  # Zeros will be overwritten by prediction values
  retailers <- cbind(retailers, predictions = 0)
  
  # creating beginning of sentence with result
  result <- paste("For the month of December", product,"are forecasted to have:")
  
  
  for (i in 1:length(retailers$retailers))
  {
    if(i != 1)
    {
      # Results for each retailers separated by comma
      result <- paste(result, ",", sep = "")
    }
    
    # getting data for creating linear model
    data_to_lm <- aggdata[which(aggdata$retailer == retailers[i,1]),]
    
    # creating linear model
    model <- lm(formula = data_to_lm$sales_value ~ data_to_lm$report_mth)
    
    # getting coefficients from the model
    a <- coef(model)[2]
    b <- coef(model)[1]
    
    # making forecasts
    retailers[i,2] <- a*(length(data_to_lm$sales_value)+1) + b
    
    # Adding a forecast for a given retailer to the resulting statement
    result <- paste(result, " $", round(retailers[i,2],2), " in ", retailers[i,1], " retailer category", sep = "")
  }

ordered <- retailers[order(retailers$predictions),] 
ordered$retailers <- factor(ordered$retailers, levels = ordered$retailers)



xxx<-ggplot(ordered, aes(x = reorder(retailers, -predictions), y = predictions, fill = retailers)) + 
  geom_bar(stat = "identity")+  xlab("Category") + ylab("Total Sales")+
  theme(strip.text.x = element_blank(),
        plot.background = element_rect(fill = "#505050"),
        panel.background = element_rect(fill = "#505050"),
        axis.title.x = element_text(color = "#D0D0D0", face = "bold"),
        axis.title.y = element_text(color = "#D0D0D0", face = "bold"),
        panel.grid.major = element_line(color = "#505050"),
        axis.text = element_text(color = "#D0D0D0", face = "bold"),
        legend.position=c(.9,.9),
        axis.text.x = element_text(angle = 45, hjust = 1)
  )

ggsave(filename="static/plot_Q3.jpg", plot=xxx, width = 15, height = 11, units = "cm")


result <- paste(result, "   ... Shall I plot it for You ?    ", sep = " ") 
  cat(result)
  
