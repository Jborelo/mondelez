#!/usr/bin/python3

#   python sendPost fiulename

import requests
import json
import sys
import os.path

# check number of arguments
if len(sys.argv) != 2:
    print ("what file should be send? ")
    sys.exit(0)

adr = 'https://mond.hopto.org:5000/webhook'
#adr = 'http://35.198.81.141:5000/webhook'
#adr = 'http://localhost:5000'

filetoPost=sys.argv[1]

# do  we really have something to send?
if not os.path.isfile(filetoPost):
    print("Can not found: " + str(filetoPost)) 
    sys.exit(0)
    
# load file to be send
with open(filetoPost) as json_file:
    toSendJson = json.load(json_file)

#print (json.dumps(toSendJson, indent=4))

print("Sending json in post to: " + str(adr))
r = requests.post(adr,  json=toSendJson)

print(r)

